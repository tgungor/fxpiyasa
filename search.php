<!doctype html>
<html lang="en">
<head>

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CSource+Sans+Pro:200,300,400,500,600,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

    <title>Fx Piyasa Portal</title>
  
  <!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Vendor CSS
	============================================ -->
	
	<link rel="stylesheet" href="font/demo-files/demo.css">

	<!-- CSS theme files
	============================================ -->
	<link rel="stylesheet" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/fontello.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">

</head>

<body>

  <div class="loader"></div>

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

      <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

      <nav id="mobile-advanced" class="mobile-advanced"></nav>

      <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

        <?php include "includes/navigation.php"; ?>


      <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

      <div class="breadcrumbs-wrap no-title">

          <div class="container">

              <ul class="breadcrumbs">

                  <li><a href="index.php">Ana Sayfa</a></li>
                  <li>Haberler</li>

              </ul>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

      <div id="content" class="page-content-wrap">

          <div class="container">

              <div class="row">

                  <main id="main" class="col-lg-9 col-md-12">

                      <h2 class="title">Forex Haberleri</h2>
                      <div class="content-element3">

                          <div class="align-center">
                              <div class="banner-wrap">

                                  <div class="banner-title"></div>
                                  <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '11'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "11"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 
                              </div>
                          </div>

                      </div>

                      <div class="content-element5">

                          <div class="entry-box style-2 list-type">
                              
                               <?php
                              

                              
                              
                              if(isset($_POST["searchbtn"])) {
                                  $search = $_POST["search"];
                                  
                                  $query = "SELECT * FROM posts WHERE post_tags LIKE '%$search%' ORDER BY post_id DESC";
                                  $search_query = mysqli_query($conn, $query);
                                  
                                  if(!$search_query) {
                                      die("QUERY FAILED:" . mysqli_error($conn));
                                  }
                                  
                                  $search_count = mysqli_num_rows($search_query);
                                  
                                  if($search_count == 0) {
                                      echo "Aramanıza ilişkin sonuç bulunamadı!";
                                  } else {
                          

						          while ($row = mysqli_fetch_assoc($search_query)){
                                  $post_id = $row["post_id"];
						          $post_category = $row["post_category"];
						          $post_title = $row["post_title"];
						          $post_text = substr($row["post_text"], 0, 200);
						          $post_image = $row["post_image"];

						  ?>
                              

                              <!-- - - - - - - - - - - - - - Entry - - - - - - - - - - - - - - - - -->
                              <div class="entry entry-small">

                                  <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->
                                  <div class="thumbnail-attachment">
                                      <a href="news_single2.php?look=<?php echo $post_id; ?>"><img src="images/<?php echo $post_image; ?>" alt=""></a>
                                      <div class="entry-label"><?php echo $post_category; ?></div>
                                  </div>

                                  <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->
                                  <div class="entry-body">

                                      <h4 class="entry-title"><a href="news_single2.php?look=<?php echo $post_id; ?>"><?php echo $post_title; ?></a></h4>
                                      <p><?php echo $post_text; ?>...</p>
                                      <a href="news_single2.php?look=<?php echo $post_id; ?>" class="btn btn-small">Haberin Devamı</a>
                                      <a href="#" class="social-btn btn btn-small icon-btn"><i class="licon-share2"></i></a>

                                  </div>

                              </div>
                              
                              <?php } 
                                  }
                              } ?>
                              


                              
                              

                          </div>

                      </div>



                  </main>

                  <aside id="sidebar" class="col-lg-3 col-md-12 sbl">

                      <!-- Widget -->
                      <div class="widget">

                          <div class="share-wrap">

                              <ul class="social-icons share v-type">

                                  <li><a href="#" class="sh-facebook"><i class="icon-facebook"></i>Facebook</a></li>
                                  <li><a href="#" class="sh-twitter"><i class="icon-twitter"></i>Twitter</a></li>
                                  <li><a href="#" class="sh-google"><i class="icon-gplus-3"></i>Google Plus</a></li>
                                  <li><a href="#" class="sh-mail"><i class="icon-mail"></i>E-mail</a></li>

                              </ul>

                          </div>

                      </div>

                      <!-- Widget -->
                      <div class="widget">

                          <div class="banner-title"></div>

                          <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '9'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "9"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                      </div>


                      <!-- Widget -->
                      <?php include "includes/widget1.php"; ?>

                      <!-- Widget -->
                      <?php include "includes/widget2.php"; ?>

                      <!-- Widget -->
                      <div class="widget">

                          <div class="banner-title"></div>

                          <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '10'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "10"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                      </div>


                  </aside>

              </div>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      <?php include "includes/footer.php"; ?>

      <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="js/libs/jquery.modernizr.js"></script>
  <script src="js/libs/jquery-2.2.4.min.js"></script>
  <script src="js/libs/jquery-ui.min.js"></script>
  <script src="js/libs/retina.min.js"></script>
  <script src="plugins/twitter/jquery.tweet.js"></script>
  <script src="plugins/jquery.queryloader2.min.js"></script>
  <script src="plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  
</body>
</html>