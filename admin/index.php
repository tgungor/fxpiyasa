<?php include "includes/admin_header.php"; ?>


    <div id="wrapper">

 <?php include "includes/admin_sidebar.php"; ?>

      <div id="content-wrapper">
        <div class="container-fluid">            
			<h1>Fx Yönetim Paneline Hoşgeldin <?php echo $_SESSION["username"]; ?></h1>
            <hr>
            <div class="row">
            <div class="col-xl-4 col-sm-6 mb-4">
              <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="far fa-file-image"></i>
                  </div>
                    <div class = "mr-5">
                        
                        <h5>Site Kısayol Linkleri</h5>
                        <hr>
                        <a class="text-white clearfix" href="../"><i class="fas fa-angle-right"></i> Ana Sayfa</a>
                        <a class="text-white clearfix" href="../news_single.php"><i class="fas fa-angle-right"></i> Oranlar</a>
                        <a class="text-white clearfix" href="../news_classic.php"><i class="fas fa-angle-right"></i> Tablolar</a>
                        <a class="text-white clearfix" href="../news_list.php"><i class="fas fa-angle-right"></i> Haberler</a>
                        <a class="text-white clearfix" href="../events_single.php"><i class="fas fa-angle-right"></i> Ekonomi Takvim</a>
                        
                                      
                
                    </div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="../index.php">
                  <span class="float-left">Siteyi Görüntüle</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-4 col-sm-6 mb-4">
              <div class="card text-white bg-primary o-hidden h-100">
                  
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="far fa-clipboard"></i>
                  </div>
                    <h5>Haber Bilgileri</h5>
                        <hr>
                    
                    <?php
                    
                    $query = "SELECT * FROM posts";
                    $select_all_posts = mysqli_query ($conn, $query);
                    $post_count = mysqli_num_rows($select_all_posts);
                    echo "<div class = 'mr-5'> {$post_count} haber yayınlandı!</div>";
                    
                    ?>
                    <br>
                    
                        <?php
                        
                        $query = "SELECT * FROM posts";
                        $query_run = mysqli_query($conn, $query);
                    

                        $qty= 0;
                        while ($num = mysqli_fetch_assoc ($query_run)) {
                        $qty += $num['post_hits'];
                        }
                        
                        echo "<div class = 'mr-5'>Toplam {$qty} kez görüntülendi!</div>";
                    
                        ?>
                    
                    
                </div>
                <a class="card-footer text-white clearfix small z-1" href="posts.php">
                  <span class="float-left">Haberleri Görüntüle</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-4 col-sm-6 mb-4">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="far fa-comment"></i>
                  </div>
                    
                    <h5>Reklam ve Kullanıcı Bilgileri</h5>
                        <hr>
                    
                    <?php
                    
                    $query = "SELECT * FROM banners";
                    $select_all_banners = mysqli_query ($conn, $query);
                    $banner_count = mysqli_num_rows($select_all_banners);
                    echo "<div class = 'mr-5'>{$banner_count} reklam yayınlandı!</div>";
                    
                    ?>
                    <br>
                    <?php
                    
                    $query = "SELECT * FROM users";
                    $select_all_users = mysqli_query ($conn, $query);
                    $user_count = mysqli_num_rows($select_all_users);
                    echo "<div class = 'mr-5'>{$user_count} kullanıcı giriş yapıldı!</div>";
                    
                    ?>

                </div>
                <a class="card-footer text-white clearfix small z-1" href="banners.php">
                  <span class="float-left">Reklamları Görüntüle</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                  </a>
                   
                    
                    <a class="card-footer text-white clearfix small z-1" href="users.php">
                    <span class="float-left">Kullanıcıları Görüntüle</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            </div>
          </div>

          <hr>
          
          <div class="container-fluid">
          <div class="row">
              <div class="col-md-6">
                        
                  <script type="text/javascript">
                    google.charts.load('current', {'packages':['bar']});
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                        ['Bilgiler', ''],
                        ['Haber', <?php echo $post_count; ?>],
                        ['Görüntülenme', <?php echo $qty; ?>],
                        ['Reklam', <?php echo $banner_count; ?>],
                        ['Kullanıcı', <?php echo $user_count; ?>],

        ]);

                    var options = {
                    chart: {
                    title: '',
                    subtitle: '',
          }
        };

                    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                    chart.draw(data, google.charts.Bar.convertOptions(options));
      }
                    </script>
              
                  
                  <div id="columnchart_material" style="width: auto; height: 400px;"></div>
                  
              </div>
              <div class="col-md-6">
                  <script type="text/javascript">
                        google.charts.load("current", {packages:["corechart"]});
                        google.charts.setOnLoadCallback(drawChart);
                      function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                        ['', ''],
                        ['Haber', <?php echo $post_count; ?>],
                        ['Görüntülenme', <?php echo $qty; ?>],
        ]);

        var options = {
          title: '',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
                  <div id="piechart_3d" style="width: auto; height: 400px;"></div>

              </div>
          </div>
          </div>
            

 <?php include "includes/admin_footer.php"; ?>