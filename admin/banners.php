<?php include "includes/admin_header.php"; ?>

<div id="wrapper">

    <?php include "includes/admin_sidebar.php"; ?>


    <div id="content-wrapper">
        <div class="container-fluid">
            <h1>Reklam Yönetim Paneli</h1>
            <hr>

            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Kategori</th>
                        <th>Resim</th>
                        <th>Link</th>
                        <th>Etiketler</th>
                        <th>İşlemler</th>
                    </tr>
                </thead>
                <tbody>
                    
                    
                    <?php
					if(isset($_POST["edit_banner"])) {
					   $banner_link = $_POST["banner_link"];
                       $banner_tags = $_POST["banner_tags"];
					                           
                       $banner_image = $_FILES["banner_image"]["name"];
				       $banner_image_temp = $_FILES["banner_image"]["tmp_name"];

				       move_uploaded_file($banner_image_temp, "../images/$banner_image");

						if(empty($banner_image)) {
							$query = "SELECT * FROM banners WHERE banner_id = '$_POST[banner_id]'";
							$select_image = mysqli_query($conn, $query);

						while($row = mysqli_fetch_array($select_image)) {
							$banner_image = $row["banner_image"];
							}
						}


					$sql_query2 = "UPDATE banners SET banner_link= '$banner_link', banner_tags = '$banner_tags', banner_image = '$banner_image' WHERE banner_id = '$_POST[banner_id]'";
					
					$edit_banner_query = mysqli_query($conn, $sql_query2);
					header("Location: banners.php");
					}

				?>

                    
                <?php 
				
				$sql_query = "SELECT * FROM banners ";
				$select_all_banners = mysqli_query($conn, $sql_query);
					$k = 1;
					while ($row = mysqli_fetch_assoc($select_all_banners)){
						$banner_id = $row["banner_id"];
						$banner_category = $row["banner_category"];
                        $banner_image = $row["banner_image"];
                        $banner_link = $row["banner_link"];
						$banner_tags = $row["banner_tags"];
						                        
                    echo "<tr>
                        <td>{$banner_id}</td>
                        <td>{$banner_category}</td>
                        <td>{$banner_image}</td>
                        <td>{$banner_link}</td>
                        <td>{$banner_tags}</td>
                        <td>
                        <a class='btn btn-large btn-primary' data-toggle='modal' data-target='#edit_modal$k' href='#'>Düzenle</a>
                        </td>
                    </tr>";
                        
                    ?>




                    <div id="edit_modal<?php echo $k; ?>" class="modal fade">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Reklam Düzenle</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <img width="100" src="../images/<?php echo $banner_image; ?>">
                                            <input type="file" class="form-control" name="banner_image">
                                        </div>
                                        <div class="form-group">
                                            <label for="banner_link">Link</label>
                                            <input type="text" class="form-control" name="banner_link" value="<?php echo $banner_link; ?>">
                                        </div>  
                                        <div class="form-group">
                                            <label for="banner_tags">Etiketler</label>
                                            <input type="text" class="form-control" name="banner_tags" value="<?php echo $banner_tags; ?>">
                                        </div>

                                    
                                        <div class="form-group">
                                            <input type="hidden" name="banner_id" value="<?php echo $row["banner_id"]; ?>">
                                            <input type="submit" class="btn btn-primary" name="edit_banner" value="Kaydet">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php $k++; } ?>
                </tbody>
            </table>
            

        

            <?php include "includes/admin_footer.php"; ?>