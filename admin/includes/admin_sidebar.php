      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Kontrol Paneli</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="posts.php">
            <i class="far fa-clipboard"></i>
            <span>Haber Yönetimi</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="banners.php">
            <i class="far fa-file-image"></i>
            <span>Reklam Yönetimi</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="users.php">
            <i class="far fa-user"></i>
            <span>Kullanıcı Yönetimi</span></a>
        </li>

      </ul>