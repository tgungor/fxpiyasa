<?php include "includes/admin_header.php"; ?>

<div id="wrapper">

    <?php include "includes/admin_sidebar.php"; ?>


    <div id="content-wrapper">
        <div class="container-fluid">
            
			<h1>Haber Yönetim Paneli</h1>
            <hr>

            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Başlık</th>
                        <th>Alt Başlık</th>
                        <th>Kategori</th>
                        <th>Resim</th>
                        <th>İçerik</th>
                        <th>Alt İçerik</th>
                        <th>Etiketler</th>
                        <th>Görüntüleme</th>
                        <th>İşlemler</th>
                    </tr>
                </thead>
                <tbody>

				<?php
				if(isset($_POST["add_post"])) {
					$post_title = $_POST["post_title"];
                    $post_subtitle = $_POST["post_subtitle"];
					$post_category = $_POST["post_category"];
					$post_tags = $_POST["post_tags"];
					$post_text = $_POST["post_text"];
                    $post_subtext = $_POST["post_subtext"];

					$post_image = $_FILES["post_image"]["name"];
					$post_image_temp = $_FILES["post_image"]["tmp_name"];

					move_uploaded_file($post_image_temp, "../images/$post_image");

					$query = "INSERT INTO posts (post_title, post_subtitle, post_category, post_tags, post_text, post_subtext, post_image)";
					$query .= "VALUES('{$post_title}', '{$post_subtitle}', '{$post_category}', '{$post_tags}', '{$post_text}', '{$post_subtext}', '{$post_image}')";

					$create_post_query = mysqli_query($conn, $query);
					header("Location: posts.php");
				}

				?>

				<?php
					if(isset($_POST["edit_post"])) {
						$post_title = $_POST["post_title"];
                        $post_subtitle = $_POST["post_subtitle"];
						$post_category = $_POST["post_category"];
						$post_tags = $_POST["post_tags"];
						$post_text = $_POST["post_text"];
                        $post_subtext = $_POST["post_subtext"];

						$post_image = $_FILES["post_image"]["name"];
						$post_image_temp = $_FILES["post_image"]["tmp_name"];

						move_uploaded_file($post_image_temp, "../images/$post_image");

						if(empty($post_image)) {
							$query = "SELECT * FROM posts WHERE post_id = '$_POST[post_id]'";
							$select_image = mysqli_query($conn, $query);

						while($row = mysqli_fetch_array($select_image)) {
							$post_image = $row["post_image"];
							}
						}
					
					$sql_query2 = "UPDATE posts SET post_title = '$post_title', post_subtitle = '$post_subtitle', post_category = '$post_category', post_tags = '$post_tags', post_text = '$post_text', post_subtext = '$post_subtext', post_image = '$post_image' WHERE post_id = '$_POST[post_id]'";
					
					$edit_post_query = mysqli_query($conn, $sql_query2);
					header("Location: posts.php");
					}

				?>


				<?php 
				
				$sql_query = "SELECT * FROM posts ORDER BY post_id DESC";
				$select_all_posts = mysqli_query($conn, $sql_query);
					$k = 1;
					while ($row = mysqli_fetch_assoc($select_all_posts)){
						$post_id = $row["post_id"];
						$post_category = $row["post_category"];
						$post_title = $row["post_title"];
                        $post_subtitle = $row["post_subtitle"];
						$post_text = substr($row["post_text"], 0, 100);
                        $post_subtext = substr($row["post_subtext"], 0, 100);
						$post_image = $row["post_image"];
						$post_tags = $row["post_tags"];
                        $post_hits = $row["post_hits"];

						echo "<tr>
                        <td>{$post_id}</td>
                        <td>{$post_title}</td>
                        <td>{$post_subtitle}</td>
                        <td>{$post_category}</td>
                        <td>{$post_image}</td>
                        <td>{$post_text}</td>
                        <td>{$post_subtext}</td>
                        <td>{$post_tags}</td>
                        <td>{$post_hits}</td>
                        <td>
                            <div class='dropdown'>
                                <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                    Seçiniz
                                </button>
                                <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                                    <a class='dropdown-item' data-toggle='modal' data-target='#edit_modal$k' href='#'>Düzenle</a>
                                    <div class='dropdown-divider'></div>
                                    <a class='dropdown-item' href='posts.php?delete={$post_id}'>Sil</a>
                                    <div class='dropdown-divider'></div>
                                    <a class='dropdown-item' data-toggle='modal' data-target='#add_modal'>Ekle</a>
                                </div>
                            </div>
                        </td>
                    </tr>";

				?>

                    <div id="edit_modal<?php echo $k; ?>" class="modal fade">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Haber Düzenle</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="post_title">Başlık</label>
                                            <input type="text" class="form-control" name="post_title" value="<?php echo $post_title; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="post_subtitle">Alt Başlık</label>
                                            <input type="text" class="form-control" name="post_subtitle" value="<?php echo $post_subtitle; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="post_category">Kategori</label>
                                            <input type="text" class="form-control" name="post_category" value="<?php echo $post_category; ?>">
                                        </div>
                                        <div class="form-group">
                                            <img width="100" src="../images/<?php echo $post_image; ?>">
                                            <input type="file" class="form-control" name="post_image">
                                        </div>
                                        <div class="form-group">
                                            <label for="post_tags">Etiketler</label>
                                            <input type="text" class="form-control" name="post_tags" value="<?php echo $post_tags; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="post_text">İçerik</label>
                                            <textarea class="form-control" name="post_text" id="" cols="20" rows="5"><?php echo $row["post_text"] ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="post_subtext">Alt İçerik</label>
                                            <textarea class="form-control" name="post_subtext" id="" cols="20" rows="5"><?php echo $row["post_subtext"] ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" name="post_id" value="<?php echo $row["post_id"]; ?>">
                                            <input type="submit" class="btn btn-primary" name="edit_post" value="Kaydet">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

					<?php $k++; } ?>

                </tbody>
            </table>
            
            <a class="btn btn-large btn-primary text-white" data-toggle="modal" data-target="#add_modal">Yeni Haber Ekle</a>

            <div id="add_modal" class="modal fade">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Haber Ekle</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="post_title">Başlık</label>
                                    <input type="text" class="form-control" name="post_title">
                                </div>
                                 <div class="form-group">
                                    <label for="post_subtitle">Alt Başlık</label>
                                    <input type="text" class="form-control" name="post_subtitle">
                                </div>
                                <div class="form-group">
                                    <label for="post_category">Kategori</label>
                                    <input type="text" class="form-control" name="post_category">
                                </div>
                                <div class="form-group">
                                    <label for="post_image">Resim</label>
                                    <input type="file" class="form-control" name="post_image">
                                </div>
                                <div class="form-group">
                                    <label for="post_tags">Etiketler</label>
                                    <input type="text" class="form-control" name="post_tags">
                                </div>
                                <div class="form-group">
                                    <label for="post_text">İçerik</label>
                                    <textarea class="form-control" name="post_text" id="" cols="20" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="post_subtext">Alt İçerik</label>
                                    <textarea class="form-control" name="post_subtext" id="" cols="20" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="post_id" value="">
                                    <input type="submit" class="btn btn-primary" name="add_post" value="Kaydet">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

			<?php
				if(isset($_GET["delete"])){

				$del_post_id = $_GET["delete"];

				$sql_query = "DELETE FROM posts WHERE post_id = {$del_post_id}";

				$delete_post_query = mysqli_query($conn, $sql_query);
				header("Location: posts.php");
			}

			?>



            <?php include "includes/admin_footer.php"; ?>