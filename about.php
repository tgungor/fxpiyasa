﻿<!doctype html>
<html lang="en">
<head>

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CSource+Sans+Pro:300,400,500,600,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

    <title>Fx Piyasa Portal</title>
  
  <!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Vendor CSS
	============================================ -->
	
	<link rel="stylesheet" href="font/demo-files/demo.css">
  <link rel="stylesheet" href="plugins/fancybox/jquery.fancybox.css">

	<!-- CSS theme files
	============================================ -->
	<link rel="stylesheet" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/fontello.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">

</head>

<body>

  <div class="loader"></div>

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

      <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

      <nav id="mobile-advanced" class="mobile-advanced"></nav>

      <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

        <?php include "includes/navigation.php"; ?>

      <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

      <div class="breadcrumbs-wrap">

          <div class="container">

              <h1 class="page-title">Hakkımızda</h1>

              <ul class="breadcrumbs">

                  <li><a href="index.php">Ana Sayfa</a></li>
                  <li>Hakkımızda</li>

              </ul>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

      <div id="content">

          <div class="page-section">

              <div class="container">

                  <div class="row">
                      <div class="col-lg-6 col-md-12">

                          <h2 class="section-title">Forex never rests</h2>
                          <p class="text-size-medium black">Forex Traders do sleep but markets don’t. When a trader has just woke up and logs on in Boston, another one located in Tokyo is winding down for the day, while a third one in London finishes lunch and waits for Wall Street’s opening bell. And you are maybe yourself waiting for an economic figure to be released or thinking about where to put the stop loss on your last trade.</p>
                          <p class="text-size-medium black">Real-time exchange rates and charts, Forex news, economic calendar, market analysis, trading newsletters, customizable technical studies, live webinars with the most renowned experts of the currency market. Further, traders can sharpen their skills in our Learning Center, compare brokers or just reach out to our community network.</p>

                      </div>
                      <div class="col-lg-6 col-md-12">

                          <div class="video-holder">

                              <iframe width="560" height="400" src="https://www.youtube.com/embed/mvu5uBdvHa4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                          </div>

                      </div>
                  </div>

              </div>

          </div>

          <div class="page-section-bg">

              <div class="container">

                  <div class="content-element5">

                      <div class="align-center">

                          <h2 class="section-title">5 Reasons to Working With Us</h2>

                      </div>

                  </div>

                  <div class="icons-box style-1 type-2">

                      <div class="row">

                          <div class="col-lg-4 col-md-6 col-sm-12">

                              <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                              <div class="icons-wrap">

                                  <div class="icons-item">
                                      <div class="item-box">
                                          <i class="licon-vault"></i>
                                          <h5 class="icons-box-title"><a href="#">TEAMWORK</a></h5>
                                          <p> Together we can achieve more</p>
                                      </div>
                                  </div>

                              </div>

                          </div>
                          <div class="col-lg-4 col-md-6 col-sm-12">

                              <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                              <div class="icons-wrap">

                                  <div class="icons-item">
                                      <div class="item-box">
                                          <i class="licon-document"></i>
                                          <h5 class="icons-box-title"><a href="#">INNOVATION</a></h5>
                                          <p>We never stop trying to find ways to create</p>
                                      </div>
                                  </div>

                              </div>

                          </div>
                          <div class="col-lg-4 col-md-6 col-sm-12">

                              <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                              <div class="icons-wrap">

                                  <div class="icons-item">
                                      <div class="item-box">
                                          <i class="licon-shield-check"></i>
                                          <h5 class="icons-box-title"><a href="#">VISION</a></h5>
                                          <p>We want our readers to have a clear view of the market</p>
                                      </div>
                                  </div>

                              </div>

                          </div>
                          <div class="col-lg-4 col-md-6 col-sm-12">

                              <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                              <div class="icons-wrap">

                                  <div class="icons-item">
                                      <div class="item-box">
                                          <i class="licon-bag-dollar"></i>
                                          <h5 class="icons-box-title"><a href="#">FEEDBACK</a></h5>
                                          <p>We rely on feedback to find ways to improve</p>
                                      </div>
                                  </div>

                              </div>

                          </div>
                          <div class="col-lg-4 col-md-6 col-sm-12">

                              <!-- - - - - - - - - - - - - - Icon Box Item - - - - - - - - - - - - - - - - -->
                              <div class="icons-wrap">

                                  <div class="icons-item">
                                      <div class="item-box">
                                          <i class="licon-cart-exchange"></i>
                                          <h5 class="icons-box-title"><a href="#">TRANSPARENCY</a></h5>
                                          <p>We want to level the playing field for retail traders being honest and transparent</p>
                                      </div>
                                  </div>

                              </div>

                          </div>


                      </div>

                  </div>

              </div>

          </div>

          <div class="page-section">

              <div class="container">

                  <div class="info-boxes">

                      <div class="row">
                          <div class="col-lg-4 col-md-12">

                              <div class="info-box">

                                  <div class="box-img">
                                      <img src="images/430x234_img1.jpg" alt="">
                                  </div>

                                  <h4 class="box-title">Our Mission</h4>
                                  <p>Create opportunities of professional growth for our audience, from beginners to advanced Forex traders, and for our collaborators and partners (banks, educators, independent analysts, data providers and brokers).</p>

                              </div>

                          </div>
                          <div class="col-lg-4 col-md-12">

                              <div class="info-box">

                                  <div class="box-img">
                                      <img src="images/430x234_img2.jpg" alt="">
                                  </div>

                                  <h4 class="box-title">Our Vision</h4>
                                  <p>Be the industry leader provider of information and resources for Forex traders providing an objective view of the markets.</p>

                              </div>

                          </div>
                          <div class="col-lg-4 col-md-12">

                              <div class="info-box">

                                  <div class="box-img">
                                      <img src="images/430x234_img3.jpg" alt="">
                                  </div>

                                  <h4 class="box-title">Our Values</h4>
                                  <p>Teamwork <br /> Innovation <br /> Vision <br /> Feedback <br />Transparency</p>

                              </div>

                          </div>
                      </div>

                  </div>

              </div>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      <?php include "includes/footer.php"; ?>

      <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="js/libs/jquery.modernizr.js"></script>
  <script src="js/libs/jquery-2.2.4.min.js"></script>
  <script src="js/libs/jquery-ui.min.js"></script>
  <script src="js/libs/retina.min.js"></script>
  <script src="plugins/fancybox/jquery.fancybox.min.js"></script>
  <script src="plugins/jquery.queryloader2.min.js"></script>
  <script src="plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  
</body>
</html>