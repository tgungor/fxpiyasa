﻿<!doctype html>
<html lang="en">
<head>

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CSource+Sans+Pro:200,300,400,500,600,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

    <title>Fx Piyasa Portal</title>
  
  <!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Vendor CSS
	============================================ -->
	
	<link rel="stylesheet" href="font/demo-files/demo.css">

	<!-- CSS theme files
	============================================ -->
	<link rel="stylesheet" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/fontello.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">

</head>

<body>

  <div class="loader"></div>

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

      <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

      <nav id="mobile-advanced" class="mobile-advanced"></nav>

      <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

        <?php include "includes/navigation.php"; ?>


      <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

      <div class="breadcrumbs-wrap">

          <div class="container">

              <h1 class="page-title">Ekonomik Takvim</h1>

              <ul class="breadcrumbs">

                  <li><a href="index.php">Ana Sayfa</a></li>
                  <li>Ekonomi Takvim</li>

              </ul>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

      <div id="content" class="page-content-wrap">

          <div class="container">

              <div class="content-element">

                  <div class="single-event">

                      <div class="content-element2">

                          <div class="row">
                              <div class="col-lg-8 col-md-12">

                                  <div class="content-element2">

                                      <div class="content-element2">
                                          <!-- TradingView Widget BEGIN -->
                                          <div class="tradingview-widget-container">
                                              <div class="tradingview-widget-container__widget"></div>
                                              <div class="tradingview-widget-copyright"><a href="#" rel="noopener" target="_blank"><span class="blue-text"></span></a></div>
                                              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-events.js" async>
                                                  {
                                                      "colorTheme": "light",
                                                          "isTransparent": false,
                                                              "width": "890",
                                                                  "height": "660",
                                                                      "locale": "tr",
                                                                          "importanceFilter": "-1,0,1"
                                                  }
                                              </script>
                                          </div>
                                          <!-- TradingView Widget END -->
                                      </div>

                                  </div>

                                  <div class="content-element3">

                                      <p class="text-black">Eş zamanlı ekonomik takvim, dünya çapındaki finansal etkinlikleri ve göstergeleri kapsar. Yeni bir bilgi yayınlandığı zaman otomatik olarak güncellenir. Eş zamanlı ekonomik takvim sadece genel veri sağlamaktadır ve işlem yapmak için bir kılavuz değildir.. Firmamız, en doğru içerikleri sağlamayı taahhüt eder fakat çok sayıda veri ve resmi kaynağın bulunması nedeniyle, oluşabilecek nihai hatalar ve tutarsızlıklardan sorumlu tutulamaz. Ayrıca eş zamanlı ekonomik takvim, uyarı yapmadan bir değişiklikliğe tabidir.</p>

                                  </div>

                                  <div class="share-wrap">

                                      <span class="share-title">Paylaş:</span>
                                      <ul class="social-icons share">

                                          <li><a href="#" class="sh-facebook"><i class="icon-facebook"></i>Facebook</a></li>
                                          <li><a href="#" class="sh-twitter"><i class="icon-twitter"></i>Twitter</a></li>
                                          <li><a href="#" class="sh-google"><i class="icon-gplus-3"></i>Google Plus</a></li>
                                          <li><a href="#" class="sh-mail"><i class="icon-mail"></i>E-mail</a></li>

                                      </ul>

                                  </div>

                                  <br />

                                  <div class="content-element2">
                                      <h4>Piyasa Takvimi ve Forex Verileri</h4>
                                      <p class="text-black">
                                          Temel analizin ana unsurlarından biri olan ekonomik takvim takibi, piyasada işlem gören varlıkların fiyatlarındaki değişimleri anlayabilmek ve söz konusu bu değişimlerden fayda sağlayabilmek adına büyük önem taşımaktadır.Ekonomik takvim, yatırımcılara, hangi ülkeden, hangi verinin, ne zaman açıklanacağı ile ilgili değerli bilgiler verirken, iktisadi göstergelerin önem derecesine ilişkin de kolaylıklar sağlamaktadır.
                                      </p>
                                      <p class="text-black">
                                          Parite ve emtia fiyatlarında sert hareketlerin oluşmasına neden olabilecek ekonomik takvim verilerinin açıklanan değerleri, tahmin rakamlarından uzaklaştıkça piyasa katılımcılarının verdiği tepkilerin de arttığı gözlemlenmektedir. Tahminlerin dışında oluşan rakamlar yatırımcıların yeni veriyi fiyatlamaya başlamasına neden olduğu için, varlık fiyatlarında sert hareketler gerçekleşmektedir.
                                      </p>
                                  </div>
                                  <div class="content-element2">
                                      <h4>Neden Kullanmalıyım?</h4>
                                      <p class="text-black">
                                          Forex piyasasındaki, en eksiksiz, en doğru ve güncel eş zamanlı ekonomik takvimdir. Tüm bu verileri günde 24 saat, haftada 5 gün güncelleyen ekonomistlerden ve gazetecilerden oluşan, kendini adamış bir ekibe sahibiz.

                                          Bu da, güvenilir, saygı duyulan ve yaygın şekilde kullanıldığına bir kanıttır.
                                      </p>
                                      <p class="text-black">
                                          Eğer siz de, temel ya da haber sunucusu iseniz, sizin için olmazsa olmazdır. Temel analiz aracılığı ile Forex işlemleri yapmak için, dünya çapındaki ekonomilerin, makro ekonomik veriler (GSYİH, istihdam, tüketim verileri, enflasyon gibi) üzerinden, ne durumda olduklarını kontrol etmeli, ve en fazla işlem yaptığınız ülkelerin para birimlerini yakından takip etmelisiniz.

                                          Ekonomik takvimimiz, yoldaşınızdır, bilgisayarınızda her zaman açık olacak bir sekmedir.
                                      </p>
                                  </div>

                              </div>
                              <div class="col-lg-4 col-md-12">

                                  <!-- Widget -->
                                  <div class="widget">

                                      <div class="banner-title"></div>

                                      <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '12'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "12"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                                  </div>

                                  <!-- Widget -->
                                  <?php include "includes/widget1.php"; ?>

                                  <!-- Widget -->
                                  <div class="widget">

                                      <div class="banner-title"></div>

                                      <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '13'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "13"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                                  </div>

                              </div>
                          </div>

                      </div>

                  </div>

              </div>

              <div class="content-element2">

                  <h4 class="title">Nasıl Yapılır?</h4>
                  <p class="text-black">
                      Bütün veriler kronolojik sırada ve günlere bölünerek görüntülenir. Yayınlanan veriler, bir tik () ile, “kalan zaman” sütunu altında işaretlenir. Açık gri yatay bir çizgi, size şuan nerede olduğunuzu gösterirken, o çizginin altında ise gelmekte olan tüm veriler gösterilir. Hızlı şekilde alabilmeniz için, bir sonraki verinin yayınlanmasına ne kadar süre kaldığı belirtilmektedir. Yeni veri yayınlandığı zaman, veriyi karçırmamanız için takvim sayfası otomatik olarak yenilenir. Eğer isterseniz, bu yayınlar için ses bildirisini açabilirsiniz.
                  </p>
                  <p class="text-black">
                      Bayrak simgesi, verinin yayınlandığı ülkeyi işaret eder, ve onun yanında da para birimi bulunmaktadır. Dolayısıyla bugün ya da bazı belirli günlerde hangi para birimleri etkilenmiş olabileceğini hızlı şekilde tarayabilir ve görebilirsiniz.
                  </p>
                  <p class="text-black">
                      Bütün ekonomik takvim göstergeleri için, Önceki miktarı bulabilirsiniz: bu son yayınlanmasındaki veridir (veri yayınlanmasının sıklığı çeşitlilik gösterir: önceki ay, önceki üç aylık dönem vs. olabilir). Çoğu gösterge için, Beklenti numarası ekleriz: bu rakamın sonucu üzerine uzmanların genel fikir birliğidir. Veri yayınlandığı zaman, volatilite göstergesinin sağında hemen gösterilir. Beklenilenden daha mı iyi yoksa daha mı kötü? Eğer yayınlanmış bir konsensüs varsa, bu ya yeşil renkte (yani veri beklenilenden daha iyi) ya da kırmızıda (beklenilenden daha kötü) olarak gelmiş demektir.
                  </p>

              </div>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      <?php include "includes/footer.php"; ?>

      <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="js/libs/jquery.modernizr.js"></script>
  <script src="js/libs/jquery-2.2.4.min.js"></script>
  <script src="js/libs/jquery-ui.min.js"></script>
  <script src="js/libs/retina.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
  <script src="plugins/jquery.queryloader2.min.js"></script>
  <script src="plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  
</body>
</html>