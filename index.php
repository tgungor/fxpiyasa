﻿<!doctype html>
<html lang="en">
<head>

    <!-- Google Web Fonts
    ================================================== -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CSource+Sans+Pro:200,300,400,500,600,700,900" rel="stylesheet" />

    <!-- Basic Page Needs
    ================================================== -->

    <title>Fx Piyasa Portal</title>

    <!--meta info-->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Vendor CSS
    ============================================ -->

    <link rel="stylesheet" href="font/demo-files/demo.css" />

    <!-- CSS theme files
    ============================================ -->
    <link rel="stylesheet" href="css/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="css/fontello.css" />
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />

</head>

<body>

  <div class="loader"></div>

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    <nav id="mobile-advanced" class="mobile-advanced"></nav>

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

    <?php include "includes/navigation.php"; ?>

    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
    
    <!-- - - - - - - - - - - - - - market info - - - - - - - - - - - - - - - - -->

    <div class="market-info">

      <div class="container">

          <div class="market-items">
              <!-- TradingView Widget BEGIN -->
              <div class="tradingview-widget-container">
                  <div class="tradingview-widget-container__widget"></div>
                  <div class="tradingview-widget-copyright"><a href="#" rel="noopener" target="_blank"><span class="blue-text"></span></a></div>
                  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
                      {
                          "symbols": [
                              {
                                  "proName": "FOREXCOM:SPXUSD",
                                  "title": "S&P 500"
                              },
                              {
                                  "proName": "FOREXCOM:NSXUSD",
                                  "title": "Nasdaq 100"
                              },
                              {
                                  "proName": "FX_IDC:EURUSD",
                                  "title": "EUR/USD"
                              },
                              {
                                  "proName": "BITSTAMP:BTCUSD",
                                  "title": "BTC/USD"
                              },
                              {
                                  "proName": "BITSTAMP:ETHUSD",
                                  "title": "ETH/USD"
                              },
                              {
                                  "description": "GBP/USD",
                                  "proName": "OANDA:GBPUSD"
                              },
                              {
                                  "description": "USD/JPY",
                                  "proName": "OANDA:USDJPY"
                              },
                              {
                                  "description": "USD/CHF",
                                  "proName": "OANDA:USDCHF"
                              },
                              {
                                  "description": "USD/CAD",
                                  "proName": "OANDA:USDCAD"
                              },
                              {
                                  "description": "AUD/USD",
                                  "proName": "OANDA:AUDUSD"
                              },
                              {
                                  "description": "EUR/GBP",
                                  "proName": "OANDA:EURGBP"
                              }
                          ],
                              "showSymbolLogo": true,
                                  "colorTheme": "light",
                                      "isTransparent": true,
                                          "displayMode": "compact",
                                              "locale": "tr"
                      }
                  </script>
              </div>
              <!-- TradingView Widget END -->
          </div>
        
      </div>

    </div>

    <!-- - - - - - - - - - - - - end market info - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap2 no-tps">

      <div class="container">
        
        <div class="content-element5">
          
          <div class="row">
              <aside id="sidebar" class="sticky-bar col-lg-2 col-md-12">

                  <!-- Widget -->

                  <div class="widget">

                      <div class="share-wrap">

                          <ul class="social-icons share v-type">

                              <li><a href="#" class="sh-facebook"><i class="icon-facebook"></i>Facebook</a></li>
                              <li><a href="#" class="sh-twitter"><i class="icon-twitter"></i>Twitter</a></li>
                              <li><a href="#" class="sh-google"><i class="icon-gplus-3"></i>Google Plus</a></li>
                              <li><a href="#" class="sh-mail"><i class="icon-mail"></i>E-mail</a></li>

                          </ul>

                      </div>

                  </div>

                  <!-- Widget -->
                  <div class="widget">

                      <div class="banner-title"></div>
                      
                      

                      <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '1'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "1"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                  </div>

              </aside>

            <main id="main" class="col-lg-10 col-md-12">
              
              <div class="row no-gutters">
                
                <div class="main col-lg-8 col-md-12 lside">
                  
                  <div class="content-element2">
                    
                    <div class="banner-title"></div>
                    <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '3'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "3"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                  </div>

                  <div class="content-element4">
                    
                    <div class="entry-box row">
                        
                        <?php
                        
                         if(isset($_GET["page"])){
                                  $page = $_GET["page"];
                              } else {
                                  $page = "";
                              }
                              
                              if($page == "" || $page == 1) {
                                  $starter_post = 0;
                              } else {
                                  $starter_post = ($page * 5) - 5;
                              }
                              
                              $sql_query2 = "SELECT * FROM posts";
                              $look_all_post = mysqli_query($conn, $sql_query2);
                              $all_post_count = mysqli_num_rows($look_all_post);
                              $page_number = ceil ($all_post_count / 5);
                        

						  $sql_query = "SELECT * FROM posts ORDER BY post_id DESC LIMIT $starter_post, 12";
						  $select_all_posts = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_posts)){
                          $post_id = $row["post_id"];
						  $post_category = $row["post_category"];
						  $post_title = $row["post_title"];
						  $post_text = substr($row["post_text"], 0, 200);
						  $post_image = $row["post_image"];

						  ?>
                        
                      <div class="col-md-6">
                        
                        <!-- - - - - - - - - - - - - - Entry - - - - - - - - - - - - - - - - -->
                        <div class="entry entry-small">
                        
                          <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->
                          <div class="thumbnail-attachment">
                            <a href="news_single2.php?look=<?php echo $post_id; ?>"><img src="images/<?php echo $post_image; ?>" alt=""></a>
                            <a class="entry-label"><?php echo $post_category; ?></a>
                          </div>
                          
                          <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->
                          <div class="entry-body">

                            <h5 class="entry-title"><a href="news_single2.php?look=<?php echo $post_id; ?>"><?php echo $post_title; ?></a></h5>
                           
                          </div>

                        </div>

                      </div>
                        
                        <?php } ?>

                    </div>

                  </div>
                  
                </div>
                <aside class="sidebar sticky-bar col-lg-4 col-md-12 sbr">
                  
                  <!-- Widget -->
                    <?php include "includes/widget1.php"; ?>

                  <!-- Widget -->
                    <?php include "includes/widget2.php"; ?>
                  
                  <!-- Widget -->
                  <div class="widget">
                  
                    <div class="banner-title"></div>

                   <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '2'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "2"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                  </div>

                  
                </aside>

              </div>

            </main>
          </div>

        </div>

        <div class="align-center">
          <div class="banner-wrap">
                    
            <div class="banner-title"></div>
            <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '4'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "4"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 
          
          </div>
        </div>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <div class="page-section-bg type2">
        
      <div class="container">
        
        <h6 class="widget-title">ORANLAR / TABLOLAR</h6>

        <div class="carousel-type-3">
          
          <div class="owl-carousel products-holder" data-max-items="4" data-item-margin="30">
            
            <!-- owl-item -->
              <div class="product">

                  <!-- TradingView Widget BEGIN -->
                  <div class="tradingview-widget-container">
                      <div class="tradingview-widget-container__widget"></div>                      
                      <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js" async>
                          {
                              "symbol": "FX:EURUSD",
                                  "width": "315",
                                      "height": "312",
                                          "locale": "tr",
                                              "dateRange": "12M",
                                                  "colorTheme": "light",
                                                      "trendLineColor": "#37a6ef",
                                                          "underLineColor": "#E3F2FD",
                                                              "isTransparent": false,
                                                                  "autosize": false,
                                                                      "largeChartUrl": ""
                          }
                      </script>
                  </div>
                  <!-- TradingView Widget END -->

              </div>

            <!-- owl-item -->
              <div class="product">

                  <!-- TradingView Widget BEGIN -->
                  <div class="tradingview-widget-container">
                      <div class="tradingview-widget-container__widget"></div>                      
                      <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js" async>
                          {
                              "symbol": "OANDA:GBPUSD",
                                  "width": "315",
                                      "height": "312",
                                          "locale": "tr",
                                              "dateRange": "12M",
                                                  "colorTheme": "light",
                                                      "trendLineColor": "#37a6ef",
                                                          "underLineColor": "#E3F2FD",
                                                              "isTransparent": false,
                                                                  "autosize": false,
                                                                      "largeChartUrl": ""
                          }
                      </script>
                  </div>
                  <!-- TradingView Widget END -->

              </div>

            <!-- owl-item -->
              <div class="product">

                  <!-- TradingView Widget BEGIN -->
                  <div class="tradingview-widget-container">
                      <div class="tradingview-widget-container__widget"></div>
                      <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js" async>
                          {
                              "symbol": "OANDA:USDJPY",
                                  "width": "315",
                                      "height": "312",
                                          "locale": "tr",
                                              "dateRange": "12M",
                                                  "colorTheme": "light",
                                                      "trendLineColor": "#37a6ef",
                                                          "underLineColor": "#E3F2FD",
                                                              "isTransparent": false,
                                                                  "autosize": false,
                                                                      "largeChartUrl": ""
                          }
                      </script>
                  </div>
                  <!-- TradingView Widget END -->

              </div>

            <!-- owl-item -->
              <div class="product">

                  <!-- TradingView Widget BEGIN -->
                  <div class="tradingview-widget-container">
                      <div class="tradingview-widget-container__widget"></div>
                      <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js" async>
                          {
                              "symbol": "OANDA:AUDUSD",
                                  "width": "315",
                                      "height": "312",
                                          "locale": "tr",
                                              "dateRange": "12M",
                                                  "colorTheme": "light",
                                                      "trendLineColor": "#37a6ef",
                                                          "underLineColor": "#E3F2FD",
                                                              "isTransparent": false,
                                                                  "autosize": false,
                                                                      "largeChartUrl": ""
                          }
                      </script>
                  </div>
                  <!-- TradingView Widget END -->

              </div>

              <div class="product">

                  <!-- TradingView Widget BEGIN -->
                  <div class="tradingview-widget-container">
                      <div class="tradingview-widget-container__widget"></div>                      
                      <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js" async>
                          {
                              "symbol": "OANDA:USDCAD",
                                  "width": "315",
                                      "height": "312",
                                          "locale": "tr",
                                              "dateRange": "12M",
                                                  "colorTheme": "light",
                                                      "trendLineColor": "#37a6ef",
                                                          "underLineColor": "#E3F2FD",
                                                              "isTransparent": false,
                                                                  "autosize": false,
                                                                      "largeChartUrl": ""
                          }
                      </script>
                  </div>
                  <!-- TradingView Widget END -->

              </div>

          </div>

          <div class="align-center">
            <a href="news_single.php" class="btn">TÜM ORANLARI GÖRÜN</a>
          </div>

        </div>

      </div>

    </div>

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      <?php include "includes/footer.php"; ?>

    <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="js/libs/jquery.modernizr.js"></script>
  <script src="js/libs/jquery-2.2.4.min.js"></script>
  <script src="js/libs/jquery-ui.min.js"></script>
  <script src="js/libs/retina.min.js"></script>
  <script src="plugins/sticky-sidebar.js"></script>
  <script src="plugins/twitter/jquery.tweet.js"></script>
  <script src="plugins/charts/dygraph.js"></script>
  <script src="plugins/jquery.queryloader2.min.js"></script>
  <script src="plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  
</body>
</html>