﻿<!doctype html>
<html lang="en">
<head>

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CSource+Sans+Pro:200,300,400,500,600,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

    <title>Fx Piyasa Portal</title>
  
  <!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Vendor CSS
	============================================ -->
	
	<link rel="stylesheet" href="font/demo-files/demo.css">
  <link rel="stylesheet" href="plugins/fancybox/jquery.fancybox.css">

	<!-- CSS theme files
	============================================ -->
	<link rel="stylesheet" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/fontello.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">

</head>

<body>

  <div class="loader"></div>

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

      <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

      <nav id="mobile-advanced" class="mobile-advanced"></nav>

      <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

        <?php include "includes/navigation.php"; ?>


      <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

      <div class="breadcrumbs-wrap no-title">

          <div class="container">

              <ul class="breadcrumbs">

                  <li><a href="index.php">Ana Sayfa</a></li>
                  <li>Oranlar ve Tablolar</li>
                  <li>Tablolar</li>

              </ul>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

      <div id="content" class="page-content-wrap">

          <div class="container">

              <div class="row">

                  <main id="main" class="col-lg-9 col-md-12">

                      <div class="content-element5">

                          <div class="entry-box style-2">

                              <div class="row">

                                  <div class="col-12">

                                      <div class="align-center">
                                          <div class="banner-wrap">

                                              <div class="banner-title"></div>
                                              <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '8'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "8"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                                          </div>
                                      </div>

                                  </div>

                                  <div class="col-12">

                                      <div class="entry entry-small">

                                          <div class="entry-body">

                                              <h3 class="entry-title"><a href="#">EŞ ZAMANLI TABLOLAR</a></h3>

                                              <div class="entry-meta">

                                                  <time class="entry-date"></time>
                                                  <span></span>
                                                  <a href="#" class="entry-cat"></a>

                                              </div>
                                              <p>Forex Grafikleri, Interbank oranlarıyla (Emtialar ve Endeksler, 22 farklı zaman dilimi ve esnek çizgi araçları dahil) gerçek zamanlı olarak 1000'den fazla grafik sunar. Aynı zamanda, Lineer Regresyon, CCI, ADX ve daha birçok farklı teknik göstergeyi (70'in üzerinde) sunar.</p>
                                              <a href="#" class="btn btn-small">Daha fazlası için tıklayın</a>

                                          </div>

                                          <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->
                                          <div class="thumbnail-attachment">
                                              <!-- TradingView Widget BEGIN -->
                                              <div class="tradingview-widget-container">
                                                  <div id="tradingview_89987"></div>
                                                  <div class="tradingview-widget-copyright"><a href="#" rel="noopener" target="_blank"><span class="blue-text"></span></a></div>
                                                  <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                                  <script type="text/javascript">
                                                      new TradingView.widget(
                                                          {
                                                              "width": 953,
                                                              "height": 624,
                                                              "symbol": "OANDA:EURUSD",
                                                              "interval": "D",
                                                              "timezone": "Etc/UTC",
                                                              "theme": "light",
                                                              "style": "1",
                                                              "locale": "tr",
                                                              "toolbar_bg": "#f1f3f6",
                                                              "enable_publishing": false,
                                                              "allow_symbol_change": true,
                                                              "container_id": "tradingview_89987"
                                                          }
                                                      );
                                                  </script>
                                              </div>
                                              <!-- TradingView Widget END -->
                                          </div>

                                          <div class="thumbnail-attachment">
                                              <!-- TradingView Widget BEGIN -->
                                              <div class="tradingview-widget-container">
                                                  <div class="tradingview-widget-container__widget"></div>
                                                  <div class="tradingview-widget-copyright"><a href="#" rel="noopener" target="_blank"><span class="blue-text"></span></a></div>
                                                  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
                                                      {
                                                          "colorTheme": "light",
                                                              "dateRange": "12M",
                                                                  "showChart": true,
                                                                      "locale": "tr",
                                                                          "largeChartUrl": "",
                                                                              "isTransparent": false,
                                                                                  "showSymbolLogo": true,
                                                                                      "width": "953",
                                                                                          "height": "700",
                                                                                              "plotLineColorGrowing": "rgba(33, 150, 243, 1)",
                                                                                                  "plotLineColorFalling": "rgba(33, 150, 243, 1)",
                                                                                                      "gridLineColor": "rgba(240, 243, 250, 1)",
                                                                                                          "scaleFontColor": "rgba(120, 123, 134, 1)",
                                                                                                              "belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
                                                                                                                  "belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
                                                                                                                      "symbolActiveColor": "rgba(33, 150, 243, 0.12)",
                                                                                                                          "tabs": [
                                                                                                                              {
                                                                                                                                  "title": "Endeksler",
                                                                                                                                  "symbols": [
                                                                                                                                      {
                                                                                                                                          "s": "FOREXCOM:SPXUSD",
                                                                                                                                          "d": "S&P 500"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FOREXCOM:NSXUSD",
                                                                                                                                          "d": "Nasdaq 100"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FOREXCOM:DJI",
                                                                                                                                          "d": "Dow 30"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "INDEX:NKY",
                                                                                                                                          "d": "Nikkei 225"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "INDEX:DEU30",
                                                                                                                                          "d": "DAX Index"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FOREXCOM:UKXGBP",
                                                                                                                                          "d": "FTSE 100"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "OANDA:EURUSD",
                                                                                                                                          "d": "EUR/USD"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "OANDA:EURJPY",
                                                                                                                                          "d": "EUR/JPY"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "OANDA:EURGBP",
                                                                                                                                          "d": "EUR/GBP"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "OANDA:EURAUD",
                                                                                                                                          "d": "EUR/AUD"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "OANDA:EURCAD",
                                                                                                                                          "d": "EUR/CAD"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "BIST:XU100",
                                                                                                                                          "d": "BIST 100"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "BIST:ZPX30",
                                                                                                                                          "d": "ZIRAAT BIST 30"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "BIST:XSIST",
                                                                                                                                          "d": "BIST ISTANBUL"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "OANDA:USDCHF",
                                                                                                                                          "d": "USD/CHF"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "OANDA:USDJPY",
                                                                                                                                          "d": "USD/JPY"
                                                                                                                                      }
                                                                                                                                  ],
                                                                                                                                  "originalTitle": "Indices"
                                                                                                                              },
                                                                                                                              {
                                                                                                                                  "title": "Emtialar",
                                                                                                                                  "symbols": [
                                                                                                                                      {
                                                                                                                                          "s": "CME_MINI:ES1!",
                                                                                                                                          "d": "S&P 500"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "CME:6E1!",
                                                                                                                                          "d": "Euro"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "COMEX:GC1!",
                                                                                                                                          "d": "Gold"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "NYMEX:CL1!",
                                                                                                                                          "d": "Crude Oil"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "NYMEX:NG1!",
                                                                                                                                          "d": "Natural Gas"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "CBOT:ZC1!",
                                                                                                                                          "d": "Corn"
                                                                                                                                      }
                                                                                                                                  ],
                                                                                                                                  "originalTitle": "Commodities"
                                                                                                                              },
                                                                                                                              {
                                                                                                                                  "title": "Tahviller",
                                                                                                                                  "symbols": [
                                                                                                                                      {
                                                                                                                                          "s": "CME:GE1!",
                                                                                                                                          "d": "Eurodollar"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "CBOT:ZB1!",
                                                                                                                                          "d": "T-Bond"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "CBOT:UB1!",
                                                                                                                                          "d": "Ultra T-Bond"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "EUREX:FGBL1!",
                                                                                                                                          "d": "Euro Bund"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "EUREX:FBTP1!",
                                                                                                                                          "d": "Euro BTP"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "EUREX:FGBM1!",
                                                                                                                                          "d": "Euro BOBL"
                                                                                                                                      }
                                                                                                                                  ],
                                                                                                                                  "originalTitle": "Bonds"
                                                                                                                              },
                                                                                                                              {
                                                                                                                                  "title": "Forex",
                                                                                                                                  "symbols": [
                                                                                                                                      {
                                                                                                                                          "s": "FX:EURUSD"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FX:GBPUSD"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FX:USDJPY"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FX:USDCHF"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FX:AUDUSD"
                                                                                                                                      },
                                                                                                                                      {
                                                                                                                                          "s": "FX:USDCAD"
                                                                                                                                      }
                                                                                                                                  ],
                                                                                                                                  "originalTitle": "Forex"
                                                                                                                              }
                                                                                                                          ]
                                                      }
                                                  </script>
                                              </div>
                                              <!-- TradingView Widget END -->
                                          </div>

                                          <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->


                                      </div>

                                  </div>

                              </div>

                          </div>

                      </div>

                  </main>
                  <aside id="sidebar" class="col-lg-3 col-md-12 sbl">

                      <!-- Widget -->
                      <?php include "includes/widget2.php"; ?>

                      <!-- Widget -->
                      <div class="widget">

                          <div class="banner-title"></div>

                          <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '7'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "7"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

                      </div>

                  </aside>

              </div>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      <?php include "includes/footer.php"; ?>

      <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="js/libs/jquery.modernizr.js"></script>
  <script src="js/libs/jquery-2.2.4.min.js"></script>
  <script src="js/libs/jquery-ui.min.js"></script>
  <script src="js/libs/retina.min.js"></script>
  <script src="plugins/instafeed.min.js"></script>
  <script src="plugins/fancybox/jquery.fancybox.min.js"></script>
  <script src="plugins/twitter/jquery.tweet.js"></script>
  <script src="plugins/jquery.queryloader2.min.js"></script>
  <script src="plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  
</body>
</html>