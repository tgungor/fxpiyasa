﻿<!doctype html>
<html lang="en">
<head>

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CSource+Sans+Pro:300,400,500,600,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

    <title>Fx Piyasa Portal</title>
  
  <!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Vendor CSS
	============================================ -->
	
	<link rel="stylesheet" href="font/demo-files/demo.css">
  <link rel="stylesheet" href="plugins/fancybox/jquery.fancybox.css">

	<!-- CSS theme files
	============================================ -->
	<link rel="stylesheet" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/fontello.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">

</head>

<body>

  <div class="loader"></div>

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

      <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

      <nav id="mobile-advanced" class="mobile-advanced"></nav>

      <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

      <?php include "includes/navigation.php"; ?>

      <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

      <div class="breadcrumbs-wrap">

          <div class="container">

              <h1 class="page-title">İletişim</h1>

              <ul class="breadcrumbs">

                  <li><a href="index.php">Ana Sayfa</a></li>
                  <li>İletişim</li>

              </ul>

          </div>

      </div>

      <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

      <div id="content">

          <div class="map-section">

              <div class="container">

                  <div class="map-info">
                      <div class="row">
                          <div class="col-md-4 col-sm-12">

                              <h3 class="title">Genel Müdürlük</h3>
                              <h4 class="title">5/24 Bize Ulaşın</h4>

                              <div class="our-info var2">

                                  <div class="info-item">
                                      <i class="licon-map-marker"></i>
                                      <span class="post">Adres</span>
                                      <p>Eski Büyükdere Cad. Park Plaza Kat:14 Maslak - Sarıyer/İstanbul - TÜRKİYE</p>
                                  </div>
                                  <div class="info-item">
                                      <i class="licon-telephone"></i>
                                      <span class="post">Telefon</span>
                                      <p content="telephone=no">+90 (212) 345 0 426 (GCM)</p>
                                  </div>
                                  <div class="info-item">
                                      <i class="licon-at-sign"></i>
                                      <span class="post">E-mail</span>
                                      <p>info@firmaadi.com</p>
                                  </div>
                                  <div class="info-item">
                                      <i class="licon-clock3"></i>
                                      <span class="post">Çalışma Saatleri</span>
                                      <p>Pazartesi - Cuma: 09:00- 18:00</p>
                                  </div>

                              </div>

                          </div>
                          <div class="col-md-8 col-sm-12">

                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d770809.0860158126!2d28.45174621544522!3d41.003964328513874!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14caa7040068086b%3A0xe1ccfe98bc01b0d0!2zxLBzdGFuYnVs!5e0!3m2!1str!2str!4v1606572685623!5m2!1str!2str" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                          </div>

                      </div>
                  </div>

              </div>

          </div>

          <div class="page-section-bg">

              <div class="container">

                  <div class="content-element3">

                      <div class="align-center">

                          <p class="text-size-medium">Sizlere sunduğumuz hizmet, ürün veya servisler hakkındaki her türlü soru, görüş, öneri veya şikayetlerinizi aşağıdaki iletişim kanallarını kullanarak bize iletebilirsiniz..</p>

                      </div>

                  </div>

                  <div class="row">
                      <div class="col-lg-2"></div>
                      <div class="col-lg-8">

                          <form id="contact-form" class="contact-form">

                              <div class="row">

                                  <div class="col-md-6">

                                      <label class="required">Adınız</label>
                                      <input type="text" name="cf-name" required="">

                                  </div>

                                  <div class="col-md-6">

                                      <label class="required">E-mail Adresiniz</label>
                                      <input type="email" name="cf-email" required="">

                                  </div>

                                  <div class="col-md-6">

                                      <label>Cep Telefonunuz</label>
                                      <input type="tel">

                                  </div>
                                  <div class="col-md-6">

                                      <label>Konu</label>
                                      <input type="url">

                                  </div>
                                  <div class="col-12">

                                      <label class="required">Mesajınız</label>
                                      <textarea rows="11" name="cf-message"></textarea>

                                  </div>

                                  <div class="col-12">

                                      <div class="align-center">
                                          <button type="submit" class="btn btn-style-4" data-type="submit">Gönder</button>
                                      </div>

                                  </div>

                              </div>

                          </form>

                      </div>
                      <div class="col-lg-2"></div>
                  </div>

              </div>

          </div>






      </div>

      <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->
      <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      <?php include "includes/footer.php"; ?>

      <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="js/libs/jquery.modernizr.js"></script>
  <script src="js/libs/jquery-2.2.4.min.js"></script>
  <script src="js/libs/jquery-ui.min.js"></script>
  <script src="js/libs/retina.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyBN4XjYeIQbUspEkxCV2dhVPSoScBkIoic"></script>
  <script src="plugins/jquery.queryloader2.min.js"></script>
  <script src="plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  
</body>
</html>