    <footer id="footer" class="footer">

      <!-- top footer -->
      <div class="top-footer">
        
        <div class="container">
          
          <div class="row align-items-center">
            <div class="col-lg-3 col-md-12">
              
              <a href="index.php" class="logo"><img src="images/logo_login_dark.png" alt=""></a>

            </div>
            
            <div class="col-lg-9 col-md-12 align-right">
              
              <ul class="social-icons">

                <li><a href="#"><i class="icon-facebook"></i></a></li>
                <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-instagram"></i></a></li>
                <li><a href="#"><i class="icon-hash"></i></a></li>

              </ul>

            </div>
          </div>

        </div>
      
      </div>

      <!-- main footer -->
      <div class="main-footer">
        
        <div class="container">
          
          <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-12">
              
              <div class="widget">
                            
                  <p>Forex ve CFD (contracts for differences) ticareti yüksek oranda kayıp riski içerir ve her yatırımcı için uygun olmayabilir.</p>
                  <p>"Burada yer alan yatırım bilgi, yorum ve tavsiyeleri yatırım danışmanlığı kapsamında değildir. Yatırım danışmanlığı hizmeti, yetkili kuruluşlar tarafından kişilerin risk ve getiri tercihleri dikkate alınarak kişiye özel sunulmaktadır. Burada yer alan yorum ve tavsiyeler ise genel niteliktedir. Bu tavsiyeler mali durumunuz ile risk ve getiri tercihlerinize uygun olmayabilir. Bu nedenle, sadece burada yer alan bilgilere dayanılarak yatırım kararı verilmesi beklentilerinize uygun sonuçlar doğurmayabilir. Eğitim içeriği veya tanıtım sayfalarındaki bilgilerin kullanılması sonucu yatırımcıların uğrayabilecekleri doğrudan ve/veya dolaylı zararlardan, maddi ve manevi zararlardan, yoksun kalınan kardan ve üçüncü kişilerin uğrayabileceği zararlardan firmamız hiçbir şekilde sorumlu tutulamaz." </p>

              </div>

            </div>
            
            <div class="col-lg-3 col-md-6 col-sm-12">
              
              <div class="widget">
                
                <h6 class="widget-title">Abone Olun!</h6>
                
                <p>
                    
                    Yeni ve güncel haberlerle ilgili bilgi almak için abone olmayı unutmayın!
                </p>
                <form id="newsletter" class="newsletter">
                  <input type="email" name="newsletter-email" placeholder="E-mail adresinizi giriniz.">
                  <button type="submit" data-type="submit" class="btn btn-style-4"><i class="licon-mailbox-full"></i>Takipte Kalın!</button>
                </form>

              </div>

            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
              
              <div class="widget">
                
                <div class="banner-title"></div>

                <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '14'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "14"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

              </div>

            </div>

          </div>

        </div>

      </div>

      <div class="copyright">
          
        <p>NO Copyright © 2020 Tüm Hakları Saklıdır.</p>

      </div>

    </footer>