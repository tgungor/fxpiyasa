<?php include "includes/db.php"; ?>

<header id="header" class="header sticky-header">
      
      <!-- searchform -->

      <div class="searchform-wrap">
        <div class="vc-child h-inherit">

          <form class="search-form" action="./search.php" method="post">
            <button type="submit" class="search-button" name="searchbtn"></button>
            <div class="wrapper">
              <input type="text" name="search" placeholder="Ne Aramıştınız?">
            </div>
          </form>

          <button class="close-search-form"></button>

        </div>
      </div>
      
      <!-- top-header -->

      <div class="top-header">

        <div class="container">

          <div class="row justify-content-between align-items-center">

            <div class="col">
              
              <!-- Socials -->

              <ul class="social-icons">

                <li><a href="#"><i class="icon-facebook"></i></a></li>
                <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-instagram"></i></a></li>
                <li><a href="#"><i class="icon-hash"></i></a></li>

              </ul>

            </div>
            
            <div class="col">
              
              <!-- logo -->

              <div class="logo-wrap">

                <a href="index.php" class="logo"><img src="images/logo_login_dark.png" alt=""></a>

              </div>

            </div>

            <div class="col align-right">
              
              <a href="#" class="btn btn-style-4 btn-big"><i class="licon-mailbox-full"></i>Takipte Kalın!</a>

            </div>

          </div>

        </div>

      </div>
      
      <!-- - - - - - - - - - - - / Mobile Menu - - - - - - - - - - - - - -->

      <!--main menu-->

      <div class="menu-holder">
        
        <div class="container">
          
          <div class="menu-wrap">

            <div class="nav-item">
              
              <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->

              <nav id="main-navigation" class="main-navigation">
                <ul id="menu" class="clearfix">
                  <li><a href="index.php">ANA SAYFA</a></li>
                  <li class="dropdown"><a href="#">ORANLAR VE TABLOLAR</a>
                    <div class="sub-menu-wrap">
                      <ul>
                        <li><a href="news_single.php">ORANLAR</a></li>
                        <li><a href="news_classic.php">TABLOLAR</a></li>
                      </ul>
                    </div>
                  </li>
                  <li><a href="news_list.php?page=1">HABERLER</a></li>
                  <li><a href="events_single.php">EKONOMİ TAKVİM</a> </li>
                  <li><a href="about.php">HAKKIMIZDA</a></li>
                  <li><a href="contact.php">İLETİŞİM</a></li>
                </ul>
              </nav>

              <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->

            </div>

            <div class="search-holder">
                
              <button type="button" class="search-button"></button>
    
            </div>

          </div>

        </div>

      </div>

    </header>