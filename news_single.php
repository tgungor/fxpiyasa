﻿<!doctype html>
<html lang="en">
<head>

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CSource+Sans+Pro:300,400,500,600,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

    <title>Fx Piyasa Portal</title>
  
  <!--meta info-->
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="description" content="">

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Vendor CSS
	============================================ -->
	
	<link rel="stylesheet" href="font/demo-files/demo.css">
  <link rel="stylesheet" href="plugins/fancybox/jquery.fancybox.css">

	<!-- CSS theme files
	============================================ -->
	<link rel="stylesheet" href="css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/fontello.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">

</head>

<body>

  <div class="loader"></div>

  <!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

  <div id="wrapper" class="wrapper-container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    <nav id="mobile-advanced" class="mobile-advanced"></nav>

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

        <?php include "includes/navigation.php"; ?>


    <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
    
    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

    <div class="breadcrumbs-wrap no-title">

      <div class="container">

        <ul class="breadcrumbs">

          <li><a href="index.php">Ana Sayfa</a></li>
          <li>Oranlar ve Tablolar</li>
          <li>Oranlar</li>

        </ul>

      </div>

    </div>

    <!-- - - - - - - - - - - - - end Breadcrumbs - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content" class="page-content-wrap">

      <div class="container">
        
        <div class="row">
          
          <main id="main" class="col-lg-9 col-md-12">
          
            <div class="content-element">
              
              <div class="entry-single">
                <div class="row">
                  <div class="sidebar col-sm-4">
                                
                    <div class="widget">
                      
                      <div class="share-wrap">

                        <ul class="social-icons share v-type">
                      
                          <li><a href="#" class="sh-facebook"><i class="icon-facebook"></i>Facebook</a></li>
                          <li><a href="#" class="sh-twitter"><i class="icon-twitter"></i>Twitter</a></li>
                          <li><a href="#" class="sh-google"><i class="icon-gplus-3"></i>Google Plus</a></li>
                          <li><a href="#" class="sh-mail"><i class="icon-mail"></i>E-mail</a></li>
                
                        </ul>
                
                      </div>
                
                    </div>
                    
                    <div class="widget">
                      
                      <div class="banner-wrap">
                                  
                        <div class="banner-title"></div>
                        <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '6'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "6"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 
                      
                      </div>
                
                    </div>
                
                  </div>
                  <div class="main col-sm-8">

                      <h2 class="title"><b>FOREX ORANLARI TABLOLARI</b></h2>
                      <p class="text-size-big">Farklı piyasalardan, forex, emtia ve borsalar üzerine 1,600den fazla varlıklar için gerçek zamanlı bankalar arası forex oranları.</p>
                      <div class="content-element2">
                          <!-- TradingView Widget BEGIN -->
                          <div class="tradingview-widget-container">
                              <div class="tradingview-widget-container__widget"></div>
                              <div class="tradingview-widget-copyright"><a href="#" rel="noopener" target="_blank"><span class="blue-text"></span></a></div>
                              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-screener.js" async>
                                  {
                                      "width": "775",
                                          "height": "1000",
                                              "defaultColumn": "overview",
                                                  "defaultScreen": "general",
                                                      "market": "forex",
                                                          "showToolbar": true,
                                                              "colorTheme": "light",
                                                                  "locale": "tr",
                                                                      "isTransparent": true
                                  }
                              </script>
                          </div>
                          <!-- TradingView Widget END -->
                      </div>
                      <div class="content-element2">
                          <h4>FOREX ORANLARI TABLOSU NEDİR?</h4>
                          <p class="text-black">
                              Farklı piyasalardan, 1,600den fazla varlıklar için (forex, emtia ve borsalar ve vadeli işlemler...) canlı oranlar. Bu tablo her bir varlık için, Son, Teklif/Sor, Değişim, Yüzdelik Değişim, Açık, Yüksek ve Düşük, Trend, Aşırı-Alım/Aşırı-Satım ve Volatilite indikatörlerini sunar. Veriler, büyük mali kurumların likidite sağlayıcısı olarak çalıştığı bankalar arası piyasadan gelmektedir.

                              Bankalar arası piyasa, yüksek seviyede likiditeye sahip olmasıyla bilinir, ve dolayısıyla da oranlar ve spread’lar oldukça rekabetçidir. Forex Oran Tablosunu kullanarak, yatırımcılar, aracılarından gelen oranları karşılaştırabilir ve kendi avantajlarına kullanabilirler. Tercih ettiğiniz varlıklar ile kendi listenizi oluşturun. Uygun para birimleri, endeks ve emtia listesinden kendi portföy oran tablonuzu seçin. Listem butonu ile kaydedin ve saklayın.
                          </p>
                      </div>
                      <div class="content-element2">
                          <h4>FOREX FİYATLAR TABLOSUNU NASIL OKUNUR?</h4>
                          <p class="text-black">
                              Son/Teklif/Sor oranları canlı olarak fiyat güncellendiği zaman kırmızı renkte (aşağı ok) ya da yeşil (yukarı ok) olarak güncellenir. Aynı zamanda günün ( 0 GMT günün başlangıcı, 24 GMT günün bitişi) açık, en yüksek ve en düşük fiyatlar olarak da gösteririz. Sayfa ortasında (0 GMT olan) gün başlangıcından beri kotalanan yüzdelik varyasyonlarını ve ufak varyasyonları da görürsünüz. Aynı zamanda, teknik çalışmalarımızı en önemli pariteler üzerine de sunuyoruz:

                              Trend insikatörümüz her 15 dakikada bir güncellenir. Varlıkların şuan ki trendiniz çok güçlü şekilde Güçlü Boğa Piyasası, Hafif Boğa Piyasası, Hafif Ayı Piyasası, Güçlü Ayı Piyasası ve yatay olarak gösterir.

                              Aşırı-alınmış / Aşırı-satılmış indikatörümüz de her 15 dakikada bir güncellenir. Çapraz için şuan ki piyasa durumunu gösterir. Aşırı alınmış, Nötr ve Aşırı satışmış.

                              Volatilite endeksi şuan ki volatiliteyi (yüksek/düşük) gösterir, ya da yaklaşmakta olan periyodlar için trendi (genişleyen/daralan) gösterir. Her 15 dakikada bir güncellenir.

                              Daha fazla veri ve araçlara kolayca ulaşmak için Kısayol butonunu kullanın: Canlı Tablo, İlgili Haberler ve Teknik varlık sayfası; ki bu sayfada varlıkların detaylı analizini bulabilirsiniz.
                          </p>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </main>

          <aside id="sidebar" class="col-lg-3 col-md-12 sbl">
                  
            <!-- Widget -->
              <?php include "includes/widget1.php"; ?>

            <!-- Widget -->
              <?php include "includes/widget2.php"; ?>
            
            <!-- Widget -->
            <div class="widget">
            
              <div class="banner-title"></div>

              <?php

						  $sql_query = "SELECT * FROM banners ORDER BY banner_id DESC";
						  $select_all_banners = mysqli_query($conn, $sql_query);

						  while ($row = mysqli_fetch_assoc($select_all_banners)){
                          $banner_id = $row["banner_id"];
						  $banner_category = $row["banner_category"];
						  $banner_link = $row["banner_link"];
						  $banner_tags = $row["banner_tags"];
						  $banner_image = $row["banner_image"];

						  ?>

                      <a href="//<?php if($banner_id == '5'){echo $banner_link;}?>" target=\"_blank\" class="banner style-2"><img src="images/<?php if($banner_id == "5"){echo $banner_image;} ?>" alt=""></a>
                      
                      <?php } ?> 

            </div>

          </aside>

        </div>

      </div>
      
    </div>

    <!-- - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

      <?php include "includes/footer.php"; ?>

    <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->

  </div>

  <!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

  <!-- JS Libs & Plugins
  ============================================ -->
  <script src="js/libs/jquery.modernizr.js"></script>
  <script src="js/libs/jquery-2.2.4.min.js"></script>
  <script src="js/libs/jquery-ui.min.js"></script>
  <script src="js/libs/retina.min.js"></script>
  <script src="plugins/instafeed.min.js"></script>
  <script src="plugins/fancybox/jquery.fancybox.min.js"></script>
  <script src="plugins/twitter/jquery.tweet.js"></script>
  <script src="plugins/jquery.queryloader2.min.js"></script>
  <script src="plugins/owl.carousel.min.js"></script>

  <!-- JS theme files
  ============================================ -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  
</body>
</html>